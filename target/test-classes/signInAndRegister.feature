Feature: Sign in and Register
  As a user
  I want to test login and register functional of site
  So that I can be sure that login and register work correctly

  Scenario Outline: Check incorrect Sign-in
    Given User opens '<page>' page
    And User checks home page visibility
    When User clicks Accounts and Lists button
    And User checks sign-in page visibility
    And User inputs '<wrongEmail>' in sign-in field for email or number and clicks Enter
    Then User checks that alert notification was showed

    Examples:
      |  page                    | wrongEmail   |
      | https://www.amazon.com/  | test124      |

    Scenario Outline: Check filling not all fields on registration page
      Given User opens '<page>' page
      And User checks home page visibility
      When User clicks Accounts and Lists button
      And User checks sign-in page visibility
      And User clicks Create your Amazon account button
      And User checks register page visibility
      And User inputs '<name>' in name field
      And User inputs '<password>' in password field
      And User inputs '<password>' in re-enter password field
      And User clicks continue button
      Then User checks that alert notification under email field was showed

      Examples:
        |  page                    | name | password |
        | https://www.amazon.com/  | name | 123456   |


      Scenario Outline: Check incorrect re-enter password on registration page
        Given User opens '<page>' page
        And User checks home page visibility
        When User clicks Accounts and Lists button
        And User checks sign-in page visibility
        And User clicks Create your Amazon account button
        And User checks register page visibility
        And User inputs '<password>' in password field
        And User inputs '<incorrectPassword>' in re-enter password field
        And User clicks continue button
        Then User checks that alert notification under re-enter password field contains '<text>'

        Examples:
          |  page                     | password | incorrectPassword | text                 |
          | https://www.amazon.com/   | 123456   | 111111            | Passwords must match |