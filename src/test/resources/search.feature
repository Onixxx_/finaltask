Feature: Search
  As a user
  I want to test searching on site functional
  So that I can be sure that searching on site works correctly

 Scenario Outline: Check searching in category
   Given User opens '<page>' page
   And User checks home page visibility
   When User clicks on choosing category near search field
   And User clicks on category '<category>'
   And User inputs '<searchWord>' in search field
   And User clicks search button
   And User checks search results page visibility
   Then User checks that url of result page contains '<searchWord>'
   And User checks that url of result page contains '<category>'


   Examples:
     |  page                    | searchWord  | category  |
     | https://www.amazon.com/  | mouse       | computers |

  Scenario Outline: Check trying buying now good without sign-in
    Given User opens '<page>' page
    And User checks home page visibility
    When User clicks on choosing category near search field
    And User clicks on category '<category>'
    And User inputs '<searchWord>' in search field
    And User clicks search button
    And User checks search results page visibility
    And User clicks on first good
    And User clicks Buy now button
    Then User checks that url contains sign-in


    Examples:
      |  page                    | searchWord  | category  |
      | https://www.amazon.com/  | mouse       | computers |

   Scenario Outline: Check searching by filters
     Given User opens '<page>' page
     And User checks home page visibility
     When User clicks on choosing category near search field
     And User clicks on category '<category>'
     And User inputs '<searchWord>' in search field
     And User clicks search button
     And User checks search results page visibility
     And User clicks on brand '<brand>'
     Then User checks that all results contain '<brand>'


     Examples:
       |  page                    | searchWord  | category  | brand   |
       | https://www.amazon.com/  | keyboard       | computers | HP |

     Scenario Outline: Check sorting by price deals and promotions
       Given User opens '<page>' page
       And User checks home page visibility
       When User clicks Today's deals button
       And User checks Deals and Promotions page visibility
       And User clicks Sort by dropdown button
       And User clicks '<order>' button
       And User checks Deals and Promotions page visibility
       Then User checks that items are sorted by price in ascending order

       Examples:
         |  page                    | order       |
         | https://www.amazon.com/  | Low to High |

       Scenario Outline: Check filter items in deals in promotions by price
         Given User opens '<page>' page
         And User checks home page visibility
         When User clicks Today's deals button
         And User checks Deals and Promotions page visibility
         And User clicks filter Price Under 25
         And User checks Deals and Promotions page visibility
         Then User checks that all items have price under 25

         Examples:
           |  page                    |
           | https://www.amazon.com/  |