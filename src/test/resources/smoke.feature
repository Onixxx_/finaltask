Feature: Smoke
  As a user
  I want to test all main site functional
  So that I can be sure that site works correctly

  Scenario Outline: Check changing delivering country
    Given User opens '<page>' page
    And User checks home page visibility
    When User clicks Deliver to button
    And User checks popup dropdown button visibility
    And User clicks on choosing country
    And User clicks on country '<country>'
    And User clicks done button
    And User refreshes page
    And User checks home page visibility
    Then User checks that deliver to changes on '<country>'

    Examples:
    |  page                    | country    |
    | https://www.amazon.com/  | Turkey     |
    | https://www.amazon.com/  | Greece     |

    Scenario Outline: Check changing language
      Given User opens '<page>' page
      And User checks home page visibility
      When User clicks on changing language button
      And User checks language settings page visibility
      And User clicks '<language>' radio button
      Then User checks that page title changed on '<pageTitle>'

      Examples:
        |  page                    | language      | pageTitle             |
        | https://www.amazon.com/  | Deutsch       | Spracheinstellungen   |
        | https://www.amazon.com/  | Espa          | Idioma de preferencia |