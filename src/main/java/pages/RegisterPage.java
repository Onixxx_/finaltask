package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class RegisterPage extends BasePage{
    @FindBy(xpath = "//input[@id='ap_customer_name']")
    private WebElement nameField;
    @FindBy(xpath = "//input[@id='ap_password']")
    private WebElement passwordField;
    @FindBy(xpath = "//input[@id='ap_password_check']")
    private WebElement passwordCheckField;
    @FindBy(xpath = "//input[@id='continue']")
    private WebElement continueButton;
    @FindBy (xpath = "//div[@id='auth-email-missing-alert']")
    private WebElement alertNotificationUnderEmailField;
    @FindBy(xpath = "//div[@id='auth-password-mismatch-alert']//div[@class='a-alert-content']")
    private WebElement alertNotificationUnderReEnterPassword;

    public RegisterPage(WebDriver driver) {
        super(driver);
    }

    public void inputNameInNameField(final String name){
        nameField.sendKeys(name);
    }

    public void inputPasswordInPasswordField(final String password){
        passwordField.sendKeys(password);
    }

    public void inputPasswordInPasswordCheckField(final String password){
        passwordCheckField.sendKeys(password);
    }

    public void clickContinueButton(){
        continueButton.click();
    }

    public boolean isAlertNotificationUnderEmailFieldVisible(){
        return alertNotificationUnderEmailField.isDisplayed();
    }

    public String getTextOfAlertNotificationUnderReEnterPassword(){
        return alertNotificationUnderReEnterPassword.getText();
    }
}
