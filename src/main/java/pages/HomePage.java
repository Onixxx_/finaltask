package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class HomePage extends BasePage{
    @FindBy(xpath = "//span[@id='GLUXCountryValue']")
    private WebElement inputCountryInPopupText;
    @FindBy(xpath = "//a[@id='nav-global-location-popover-link']")
    private WebElement deliverToButton;
    @FindBy(xpath = "//span[@data-action= 'a-dropdown-button']")
    private WebElement chooseCountryDropdownButton;
    @FindBy(xpath = "//a[@class='a-dropdown-link']")
    private List<WebElement> countries;
    @FindBy(xpath = "//span[@data-action='a-popover-close']")
    private WebElement donePopupButton;
    @FindBy(xpath = "//span[@id='glow-ingress-line2']")
    private WebElement deliverToCountryText;
    @FindBy(xpath = "//a[@id='icp-nav-flyout']")
    private WebElement changingLanguageButton;
    @FindBy (xpath = "//a[@id='nav-link-accountList']")
    private WebElement toAccountsAndListsButton;
    @FindBy (xpath = "//select[@id='searchDropdownBox']")
    private WebElement selectCategoryButton;
    @FindBy(xpath = "//option[contains(@value, 'search-alias')]")
    private List<WebElement> categories;
    @FindBy (xpath = "//input[@id='twotabsearchtextbox']")
    private WebElement searchInput;
    @FindBy(xpath = "//input[@id='nav-search-submit-button']")
    private WebElement searchSubmitButton;
    @FindBy(xpath = "//a[@data-csa-c-slot-id='nav_cs_0']")
    private WebElement todayDealsButton;

    public HomePage(WebDriver driver) {
        super(driver);
    }

    public void openHomePage(final String url){
        driver.get(url);
    }

    public void clickDeliverToButton(){
        deliverToButton.click();
    }

    public void isChooseCountryDropdownButtonVisible(){
        waitForVisibilityOfElement(40, chooseCountryDropdownButton);
    }

    public void clickChooseCountryDropdownButton(){
        chooseCountryDropdownButton.click();
    }

    public void clickOnCountryButtonInDropdown(final String country) {
        for (WebElement element: countries) {
            if (element.getText().equalsIgnoreCase(country)){
                element.click();
                break;
            }
        }
    }

    public void clickDonePopupButton() {
        donePopupButton.click();
    }

    public String getDeliverToCountryText(){
        return deliverToCountryText.getText();
    }

    public void clickChangingLanguageButton(){
        changingLanguageButton.click();
    }

    public void clickToAccountsAndListsButton(){
        toAccountsAndListsButton.click();
    }

    public void clickSelectCategoryButton(){
        selectCategoryButton.click();
    }

    public void clickOnCategory(final String category){
        for (WebElement element: categories) {
            if (element.getText().equalsIgnoreCase(category)){
                element.click();
                break;
            }
        }
    }

    public void inputSearchWordInSearchInput(final String searchWord){
        searchInput.sendKeys(searchWord);
    }

    public void clickSearchSubmitButton(){
        searchSubmitButton.click();
    }

    public void clickTodayDealsButton(){
        todayDealsButton.click();
    }

    public String getInputCountryInPopupText(){
        return inputCountryInPopupText.getText();
    }
}
