package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

import static java.lang.Double.parseDouble;

public class DealsAndPromotionsPage extends BasePage{
    @FindBy(xpath = "//span[contains(@class, 'a-button-text')]")
    private WebElement sortByButton;
    @FindBy(xpath = "//a[contains(@class, 'a-dropdown-link')]")
    private List<WebElement> sortFilters;
    @FindBy(xpath = "//span[contains(@class, 'dealPriceText')]")
    private List<WebElement> sortResultItems;
    @FindBy(xpath = "//span[contains(@data-gbfilter-link, 'price')]//a[text()= 'Under $25']")
    private WebElement priceFilterUnder25;

    public DealsAndPromotionsPage(WebDriver driver) {
        super(driver);
    }

    public void clickSortByButton(){
        sortByButton.click();
    }

    public void clickSortInOrderFilter(final String order){
        for (WebElement element: sortFilters) {
            if (element.getText().contains(order)){
                element.click();
                break;
            }
        }
    }

    public boolean checkSortingInAscendingOrderOfPrice(){
        for (int i=0; i<sortResultItems.size()-1; i++) {
            double el1 = parseDouble(sortResultItems.get(i).getText().substring(1, sortResultItems.get(i+1).getText().indexOf('-')));
            double el2 = parseDouble(sortResultItems.get(i+1).getText().substring(1, sortResultItems.get(i+1).getText().indexOf('-')));
            if (el1 > el2) return false;
        }
        return true;
    }

    public void clickPriceFilterUnder25(){
        priceFilterUnder25.click();

    }

    public boolean checkPricesOfFilteredItems(){
        for (int i=0; i<sortResultItems.size()-1; i++) {
            double el1 = parseDouble(sortResultItems.get(i).getText().substring(1, sortResultItems.get(i+1).getText().indexOf('-')));
            if (el1 > 25) return false;
        }
        return true;
    }
}
