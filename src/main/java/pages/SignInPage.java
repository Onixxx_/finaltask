package pages;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class SignInPage extends BasePage{
    @FindBy(xpath = "//input[@id='ap_email']")
    private WebElement emailInputField;
    @FindBy(xpath = "//div[@id='auth-error-message-box']")
    private WebElement alertNotification;
    @FindBy(xpath = "//a[@id='createAccountSubmit']")
    private WebElement createAccountButton;

    public SignInPage(WebDriver driver) {
        super(driver);
    }

    public void inputWrongWordInEmailInputField(final String wrongEmail){
        emailInputField.sendKeys(wrongEmail);
        emailInputField.sendKeys(Keys.ENTER);
    }

    public boolean isAlertNotificationVisible(){
        return alertNotification.isDisplayed();
    }

    public void clickCreateAccountButton(){
        createAccountButton.click();
    }
}
