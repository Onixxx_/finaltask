package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class SearchResultsPage extends BasePage{
    @FindBy(xpath = "//img[@class= 's-image']")
    private List<WebElement> searchResultsImages;
    @FindBy(xpath = "//input[@id='buy-now-button']")
    private WebElement buyNowButton;
    @FindBy(xpath = "//li[contains(@id,'p_89')]//span[contains(@class, 'a-size-base')]")
    private List<WebElement> brandFilters;
    @FindBy(xpath = "//span[contains(@class,'search-results')]//span[contains(@class,'a-text-normal')]")
    private List<WebElement> searchResultsItems;

    public SearchResultsPage(WebDriver driver) {
        super(driver);
    }

    public void clickOnFirstSearchResult(){
        searchResultsImages.get(0).click();
    }

    public void clickBuyNowButton(){
        buyNowButton.click();
    }

    public void clickOnBrandFilter(final String brand){
        for (WebElement element: brandFilters) {
            if (element.getText().equalsIgnoreCase(brand)){
                element.click();
                break;
            }
        }
    }

    public boolean checkSearchResultsByBrandFilter(final String brand){
        for (WebElement element: searchResultsItems) {
            if (!(element.getText().contains(brand))){
                return false;
            }
        }
        return true;
    }
}
