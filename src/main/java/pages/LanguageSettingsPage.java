package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class LanguageSettingsPage extends BasePage{
    @FindBy(xpath = "//span[contains(@class,'a-radio-label')]")
    private List<WebElement> chooseLanguageRadioButtons;
    @FindBy(xpath = "//span[@id='lop-heading']")
    private WebElement languageSettingsPageTitle;

    public LanguageSettingsPage(WebDriver driver) {
        super(driver);
    }

    public void clickLanguageRadioButton(final String language){
        for (WebElement element: chooseLanguageRadioButtons) {
            if (element.getText().contains(language)){
                element.click();
                break;
            }
        }
    }

    public String getLanguageSettingsPageTitle(){
        return languageSettingsPageTitle.getText();
    }
}
