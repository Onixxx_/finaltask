package stepdefinitions;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import manager.PageFactoryManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import pages.*;

import static io.github.bonigarcia.wdm.WebDriverManager.chromedriver;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class DefinitionSteps {
    private static final long TIMEOUT = 40;
    private WebDriver driver;
    private PageFactoryManager pageFactoryManager;
    private HomePage homePage;
    private LanguageSettingsPage languageSettingsPage;
    private SignInPage signInPage;
    private RegisterPage registerPage;
    private SearchResultsPage searchResultsPage;
    private DealsAndPromotionsPage dealsAndPromotionsPage;

    @Before
    public void testsSetUp(){
        chromedriver().setup();
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        pageFactoryManager = new PageFactoryManager(driver);
    }

    @After
    public void tearDown() {
        driver.close();
    }

    @Given("User opens {string} page")
    public void openHomePage(final String page) {
        homePage = pageFactoryManager.getHomePage();
        homePage.openHomePage(page);
    }

    @And("User checks home page visibility")
    public void checkPageVisibility() {
        homePage.waitForPageReadyState(TIMEOUT);
    }


    @When("User clicks Deliver to button")
    public void clickDeliverToButton() {
        homePage.clickDeliverToButton();
    }

    @And("User checks popup dropdown button visibility")
    public void checkPopupDropdownButtonVisibility() {
        homePage.isChooseCountryDropdownButtonVisible();
    }

    @And("User clicks on choosing country")
    public void clickOnChooseCountryButton() {
        homePage.clickChooseCountryDropdownButton();
    }

    @And("User clicks on country {string}")
    public void clickOnCountry(final String country) {
        homePage.clickOnCountryButtonInDropdown(country);
        assertEquals(country, homePage.getInputCountryInPopupText());
    }

    @And("User clicks done button")
    public void clickDoneButton() {
        homePage.clickDonePopupButton();
    }

    @And("User refreshes page")
    public void userRefreshesPage() {
        driver.navigate().refresh();
    }

    @Then("User checks that deliver to changes on {string}")
    public void checkThatDeliverToChangesOnCountry(final String country) {
        assertEquals(country, homePage.getDeliverToCountryText());
    }

    @When("User clicks on changing language button")
    public void clickOnChangingLanguageButton() {
        homePage.clickChangingLanguageButton();
    }

    @And("User checks language settings page visibility")
    public void checkLanguageSettingsPageVisibility() {
        languageSettingsPage = pageFactoryManager.getLanguageSettingsPage();
        languageSettingsPage.waitForPageReadyState(TIMEOUT);
    }

    @And("User clicks {string} radio button")
    public void clickLanguageRadioButton(final String language) {
        languageSettingsPage.clickLanguageRadioButton(language);
    }


    @Then("User checks that page title changed on {string}")
    public void userChecksThatPageTitleChangedOnPageTitle(final String pageTitle) {
        assertEquals(pageTitle, languageSettingsPage.getLanguageSettingsPageTitle());
    }

    @When("User clicks Accounts and Lists button")
    public void clickAccountsAndListsButton() {
        homePage.clickToAccountsAndListsButton();
    }


    @And("User checks sign-in page visibility")
    public void checkSignInPageVisibility() {
        signInPage = pageFactoryManager.getSignInPage();
        signInPage.waitForPageReadyState(TIMEOUT);
    }

    @And("User inputs {string} in sign-in field for email or number and clicks Enter")
    public void inputsWrongEmailInSignInFieldForEmailOrNumberAndClickEnter(final String wrongEmail) {
        signInPage.inputWrongWordInEmailInputField(wrongEmail);
    }

    @Then("User checks that alert notification was showed")
    public void checkThatAlertNotificationWasShowed() {
        assertTrue(signInPage.isAlertNotificationVisible());
    }


    @And("User clicks Create your Amazon account button")
    public void clickCreateYourAmazonAccountButton() {
        signInPage.clickCreateAccountButton();
    }

    @And("User checks register page visibility")
    public void checkRegisterPageVisibility() {
        registerPage = pageFactoryManager.getRegisterPage();
        registerPage.waitForPageReadyState(TIMEOUT);
    }

    @And("User inputs {string} in name field")
    public void inputNameInNameField(final String name) {
        registerPage.inputNameInNameField(name);
    }

    @And("User inputs {string} in password field")
    public void inputPasswordInPasswordField(final String password) {
        registerPage.inputPasswordInPasswordField(password);
    }

    @And("User inputs {string} in re-enter password field")
    public void inputsPasswordInPasswordCheckField(final String password) {
        registerPage.inputPasswordInPasswordCheckField(password);
    }

    @And("User clicks continue button")
    public void clickContinueButton() {
        registerPage.clickContinueButton();
    }

    @Then("User checks that alert notification under email field was showed")
    public void checkThatAlertNotificationUnderEmailFieldWasShowed() {
        assertTrue(registerPage.isAlertNotificationUnderEmailFieldVisible());
    }

    @Then("User checks that alert notification under re-enter password field contains {string}")
    public void userChecksThatAlertNotificationUnderReEnterPasswordFieldContainsText(final String text) {
        assertEquals(text, registerPage.getTextOfAlertNotificationUnderReEnterPassword());
    }

    @When("User clicks on choosing category near search field")
    public void clickOnChoosingCategoryNearSearchField() {
        homePage.clickSelectCategoryButton();
    }

    @And("User clicks on category {string}")
    public void clickOnCategory(final String category) {
        homePage.clickOnCategory(category);
    }

    @And("User inputs {string} in search field")
    public void inputSearchWordInSearchField(final String searchWord) {
        homePage.inputSearchWordInSearchInput(searchWord);
    }

    @And("User clicks search button")
    public void clickSearchButton() {
        homePage.clickSearchSubmitButton();
    }

    @And("User checks search results page visibility")
    public void checkSearchResultsPageVisibility() {
        searchResultsPage = pageFactoryManager.getSearchResultsPage();
        searchResultsPage.waitForPageReadyState(TIMEOUT);
    }

    @Then("User checks that url of result page contains {string}")
    public void checkThatUrlOfResultPageContainsSearchWord(final String word) {
        assertTrue(driver.getCurrentUrl().contains(word));
    }

    @And("User clicks on first good")
    public void clickOnFirstGood() {
        searchResultsPage.clickOnFirstSearchResult();
    }

    @And("User clicks Buy now button")
    public void clickBuyNowButton() {
        searchResultsPage.clickBuyNowButton();
    }

    @Then("User checks that url contains sign-in")
    public void checkThatUrlContainsSignIn() {
        assertTrue(driver.getCurrentUrl().contains("signin"));
    }


    @And("User clicks on brand {string}")
    public void clickOnBrand(final String brand) {
        searchResultsPage.clickOnBrandFilter(brand);
    }

    @Then("User checks that all results contain {string}")
    public void checkThatAllResultsContainBrand(final String brand) {
        assertTrue(searchResultsPage.checkSearchResultsByBrandFilter(brand));
    }

    @When("User clicks Today's deals button")
    public void clickTodaySDealsButton() {
        homePage.clickTodayDealsButton();
    }

    @And("User checks Deals and Promotions page visibility")
    public void checkDealsAndPromotionsPageVisibility() {
        dealsAndPromotionsPage = pageFactoryManager.getDealsAndPromotionsPage();
        dealsAndPromotionsPage.waitForPageReadyState(TIMEOUT);
    }

    @And("User clicks Sort by dropdown button")
    public void clickSortByDropdownButton() {
        dealsAndPromotionsPage.clickSortByButton();
    }

    @And("User clicks {string} button")
    public void clickOrderButton(final String order) {
        dealsAndPromotionsPage.clickSortInOrderFilter(order);
    }

    @Then("User checks that items are sorted by price in ascending order")
    public void checkThatItemsAreSortedByPriceInAscendingOrder() {
        assertTrue(dealsAndPromotionsPage.checkSortingInAscendingOrderOfPrice());
    }

    @And("User clicks filter Price Under 25")
    public void clickFilterPricePrice() {
        dealsAndPromotionsPage.clickPriceFilterUnder25();
    }

    @Then("User checks that all items have price under 25")
    public void checkThatAllItemsHavePriceUnder() {
        assertTrue(dealsAndPromotionsPage.checkPricesOfFilteredItems());
    }
}
