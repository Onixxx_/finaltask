package manager;

import org.openqa.selenium.WebDriver;
import pages.*;

public class PageFactoryManager {
    private WebDriver driver;

    public PageFactoryManager(WebDriver driver) {
        this.driver = driver;
    }

    public HomePage getHomePage(){
        return new HomePage(driver);
    }

    public SignInPage getSignInPage(){
        return new SignInPage(driver);
    }

    public LanguageSettingsPage getLanguageSettingsPage(){
        return new LanguageSettingsPage(driver);
    }

    public RegisterPage getRegisterPage(){
        return new RegisterPage(driver);
    }

    public SearchResultsPage getSearchResultsPage(){
        return new SearchResultsPage(driver);
    }

    public DealsAndPromotionsPage getDealsAndPromotionsPage() { return new DealsAndPromotionsPage(driver); }
}
